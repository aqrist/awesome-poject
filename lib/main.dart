import 'package:adobi/ui/activity/activity.dart';
import 'package:adobi/ui/login.dart';
import 'package:adobi/ui/register.dart';
import 'package:flutter/material.dart';

void main() => runApp(MaterialApp(
  initialRoute: '/',
  routes: {
    '/' : (context) => Login(),
    'register' : (context) => Register(),
    'activity' : (context) => Activity(),
  },
  debugShowCheckedModeBanner: false,
));

