import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class Register extends StatefulWidget {
  @override
  _RegisterState createState() => _RegisterState();
}

class _RegisterState extends State<Register> {
  final _key = GlobalKey<FormState>();
  bool _obscure = true;
  String eid;
  String pwd;
  String fullname;
  String email;

  void toggleObscure() {
    setState(() {
      _obscure = !_obscure;
    });
  }

  void submit() {
    final signin = _key.currentState;
    if (signin.validate()) {
      signin.save();
      save();
    }
  }

  void save() async {
    final response = await http
        .post("http://192.168.64.2/timesheet/webservices/register.php", body: {
      "name": "name",
      "username": "username",
      "eid": eid,
      "phone": "08657861184",
      "pass": pwd,
      "email": email,
      "status": "1",
      "level": "1",
    });
    final data = jsonDecode(response.body);
    int value = data['value'];
    String message = data['message'];
    if (value == 1) {
      setState(() {
        Navigator.pop(context);
      });
      print(message);
    } else {
      print(message);
      showDialog(
          context: context,
          builder: (context) {
            return AlertDialog(
              title: Text("Peringatan"),
              content: Text(message),
            );
          });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Register"),
      ),
      body: Center(
        child: Padding(
          padding: const EdgeInsets.all(24.0),
          child: ListView(
            children: <Widget>[
              Form(
                key: _key,
                child: Column(
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: TextFormField(
                        onSaved: (a) => eid = a,
                        validator: (e) {
                          if (e.isEmpty) {
                            return "fill your employee ID";
                          }
                        },
                        keyboardType: TextInputType.emailAddress,
                        decoration: InputDecoration(
                            hintText: "Employee ID", icon: Icon(Icons.person)),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: TextFormField(
                        onSaved: (a) => email = a,
                        validator: (e) {
                          if (e.isEmpty) {
                            return "fill your email";
                          }
                        },
                        keyboardType: TextInputType.emailAddress,
                        decoration: InputDecoration(
                            hintText: "email", icon: Icon(Icons.person)),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: TextFormField(
                        onSaved: (a) => fullname = a,
                        keyboardType: TextInputType.emailAddress,
                        decoration: InputDecoration(
                            hintText: "Fullname", icon: Icon(Icons.person)),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: TextFormField(
                        onSaved: (a) => pwd = a,
                        validator: (e) {
                          if (e.isEmpty) {
                            return "fill your employee password";
                          }
                        },
                        obscureText: _obscure,
                        decoration: InputDecoration(
                          hintText: "Password",
                          icon: Icon(Icons.lock),
                          suffixIcon: InkWell(
                              onTap: () {
                                toggleObscure();
                              },
                              child: Icon(_obscure
                                  ? Icons.visibility_off
                                  : Icons.visibility)),
                        ),
                      ),
                    ),
                    SizedBox(height: 24),
                    InkWell(
                      onTap: () {
                        submit();
                      },
                      child: Container(
                        height: 42.0,
                        width: 100.0,
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(20.0),
                            gradient: LinearGradient(
                                colors: [Colors.blue, Colors.lightBlueAccent])),
                        child: Center(
                          child: Text(
                            "Sign Up",
                            style: TextStyle(color: Colors.white),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
