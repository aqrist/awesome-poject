import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class Login extends StatefulWidget {
  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> {
  final _key = GlobalKey<FormState>();
  bool _obscure = true;
  String eid;
  String pass;

  void toggleObscure() {
    setState(() {
      _obscure = !_obscure;
    });
  }

  void submit() {
    final signin = _key.currentState;
    if (signin.validate()) {
      signin.save();
      login();
    }
  }

  var loading = false;
  login() async {
    loading = true;
    final response = await http
        .post("http://192.168.64.2/timesheet/webservices/login.php", body: {
      "eid": eid,
      "pass": pass,
    });
    final data = jsonDecode(response.body);
    int value = data['value'];
    String message = data['message'];
    if (value == 1) {
      setState(() {
        loading = false;
      });
      print(message);
    } else {
      setState(() {
        loading = false;
      });
      print(message);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Padding(
          padding: const EdgeInsets.all(24.0),
          child: ListView(
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.all(24.0),
                child: Image.asset(
                  "asset/img/diamond.png",
                  height: 140.0,
                ),
              ),
              Form(
                key: _key,
                child: Column(
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: TextFormField(
                        onSaved: (a) => eid = a,
                        validator: (e) {
                          if (e.isEmpty) {
                            return "fill your employee ID";
                          }
                        },
                        keyboardType: TextInputType.emailAddress,
                        decoration: InputDecoration(
                            hintText: "Username", icon: Icon(Icons.person)),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: TextFormField(
                        onSaved: (a) => pass = a,
                        validator: (e) {
                          if (e.isEmpty) {
                            return "fill your employee password";
                          }
                        },
                        obscureText: _obscure,
                        decoration: InputDecoration(
                          hintText: "Password",
                          icon: Icon(Icons.lock),
                          suffixIcon: InkWell(
                              onTap: () {
                                toggleObscure();
                              },
                              child: Icon(_obscure
                                  ? Icons.visibility_off
                                  : Icons.visibility)),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text(
                  "Forgot Password",
                  textAlign: TextAlign.end,
                ),
              ),
              SizedBox(height: 24),
              InkWell(
                onTap: () {
                  submit();
                },
                child: Container(
                  height: 42.0,
                  width: 100.0,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(20.0),
                      gradient: LinearGradient(
                          colors: [Colors.blue, Colors.lightBlueAccent])),
                  child: Center(
                    child: Text(
                      "Log In",
                      style: TextStyle(color: Colors.white),
                    ),
                  ),
                ),
              ),
              SizedBox(
                height: 42.0,
              ),
              InkWell(
                onTap: () {
                  Navigator.pushNamed(context, 'register');
                },
                child: Center(
                  child: Text("Sign up a new account"),
                ),
              ),
              SizedBox(
                height: 10.0,
              ),
              loading
                  ? Center(
                      child: CircularProgressIndicator(),
                    )
                  : SizedBox()
            ],
          ),
        ),
      ),
    );
  }
}
